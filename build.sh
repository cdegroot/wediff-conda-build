#!/bin/bash

export PYTHON_INCLUDE_DIR=${PREFIX}/include/python${PY_VER}m
export PYTHON_LIB_DIR=${PREFIX}/lib
export PYTHON_SITE_PACKAGES=${PREFIX}/lib/python${PY_VER}/site-packages
export PYTHON_VERSION=${PY_VER}m
export BOOST_INCLUDE_DIR=${PREFIX}/include
export BOOST_LIB_DIR=${PREFIX}/lib
export WEDIFF_INST_DIR=${PREFIX}

export PATH=${PREFIX}/bin:/bin

scons -c
scons
scons install

